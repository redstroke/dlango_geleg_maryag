from django.shortcuts import render
from django.http import HttpResponse
from django.template.defaulttags import register
from .models import Datas

class Info(object):
    def __init__(self, id, sum, deleg):
        self.id = id
        self.sum = sum
        self.info = list(deleg)

    def __repr__(self):
        return f"{self.info}"

    def get_kol(self):
        return len(self.info)

    def get_inf(self):
        return self.info

    def get_sum(self):
        return self.sum

    def set_sum(self, s):
        self.sum = s

    def set_inf(self, s):
        self.info = s

I = []
I0 = Info(1, 0, [])
flag = 0


# def info(request):
#     return render(request, 'task2/info.html')

def index(request):
    if (flag!=0):
        k = Info.get_kol(I0)
    else:
        k = ''
    return render(request, 'task2/wrapper.html', {'kolvo': k})

def razdel(request):
    k = int(request.POST['kol'])
    if (flag!=0):
        k = Info.get_kol(I0)
        s = Info.get_sum(I0)
        d = Info.get_inf(I0)
    else:
        d = []
        s = ''
        for i in range(k):
            d.append('')
    return render(request, 'task2/razdel.html', {'kolvo': k, 'sum': s, 'del': d})


def rez(request):
    # print(request.POST)
    p = request.POST.getlist('part')
    part = []
    psort = []
    for pa in p:
        part.append(int(pa))
        psort.append(int(pa))
    psort.sort()
    s = int(request.POST['sum'])
    rez = deleg(s, psort, part)

    Info.set_sum(I0, s)
    Info.set_inf(I0, part)
    I.clear()
    global flag
    flag = 0
    return render(request, 'task2/rez.html', {'kolvo': len(p), 'rezult': rez})

def exx(request):
    # I.append(Info(0, 400, [300, 200, 100]))
    I0.set_inf([300, 200, 100])
    I0.set_sum(400)
    global flag
    flag = 1
    return index(request)

@register.filter
def get_in(value, arg):
    return value[arg]

@register.filter
def get_range(value):
    return range(value)

def deleg(s, p, p1):
    k = len(p)
    rez = []
    for i in range(k):
        rez.append(0)
    print(p)
    a = s/k
    f = 1
    for i in range(k):
        if f == 1:
            if a <= p[i]/2:
                for j in range(i, k):
                    rez[j] = rez[j] + a
                f = 0
            else:
                rez[i] = rez[i] + p[i]/2
                if i == k - 1:
                    b = a - rez[k-1]
                    k1 = 1
                    for i1 in range(k-1, -1, -1):
                        if f == 1:
                            if i1 > 0:
                                raz = (p[i1]/2)-(p[i1-1]/2)
                            else:
                                raz = p[0]/2
                            if b/k1 <= raz:
                                rez[i1] = rez[i1] + b/k1
                                f = 0
                                for j in range(i1+1, k, 1):
                                    rez[j] = rez[j] + b / k1
                            else:
                                rez[i1] = rez[i1]+ raz
                                k1 = k1+1
                                b = b - raz
                                for j in range(i1+1, k, 1):
                                    rez[j] = rez[j] + raz
                                    b = b - raz

                else:
                    a = a + (a - rez[i]) / (k - 1 - i)
    rez1 = []
    for i in range(k):
        for j in range(k):
            if p1[i] == p[j]:
                st = 'Доля человека, претендовавшего на ' + str(p1[i]) + ' составляет ' + str(rez[j])
                rez1.append(st)
                break
    return rez1




def create(request):
    print('RRRRRRR')
    ii = Info.get_inf(I0)
    ss = Info.get_sum(I0)
    # i = ''
    # s = str(ss)
    # for w1 in ii:
    #     i = i + str(w1) + ' '

    I1 = Datas.objects.create(summa=ss, doli=ii)
    ind = I1.id
    return render(request, 'task2/okno1.html', {'ind': ind})


def set_in(request):
    ind = request.POST.getlist('ind')
    i = Datas.objects.get(id = ind[0])

    ss = i.get_s()
    ii = i.get_i()
    sss = ''
    iii = ''
    for j in ss:
        if ((j != '[') & (j != "'") & (j != ']')):
            sss = sss + j
    for j in ii:
        if ((j != '[') & (j != "'") & (j != ']')):
            iii = iii + j

    summ = int(sss)
    infs = iii.split(', ')

    inf =[]
    for r1 in infs:
        inf.append(int(r1))

    Info.set_sum(I0, summ)
    Info.set_inf(I0, inf)
    global flag
    flag = 1
    return index(request)




def get_existing_results(request):
    print(request.POST)
    input_str1 = 'name_id'
    var1 = input_str1 in request.POST[input_str1] or request.POST
    name_id = var1.get(input_str1, None)

    input_str2 = 'n_couples'
    var2 = input_str2 in request.POST[input_str2] or request.POST
    n = int(var2.get(input_str2, None))