from django.db import models

class Datas(models.Model):
    id = models.AutoField('ID', serialize=False, primary_key=True, db_index= True, auto_created=True, unique= True)
    summa = models.TextField('Summa')
    doli = models.TextField('Deleg')

    def __str__(self):
        return f"id: {self.id} summa: {self.summa}\ndoli {self.doli}"


    def get_s(self):
        return self.summa

    def get_i(self):
        return self.doli

