from .models import Datas
from django.forms import ModelForm

class InfoForm(ModelForm):
    class Meta:
        model = Datas
        fields = ['summa', 'doli']