from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('razdel/', views.razdel, name='razdel'),
    path('exx', views.exx, name='exx'),
    path('razdel/rez/', views.rez, name='rez'),
    path('razdel/rez/id', views.create, name='new_id'),
    path('ids', views.set_in, name='id_s'),
]