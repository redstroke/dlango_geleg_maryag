from django.urls import path, include
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('ex', views.ex, name='ex'),
    path('names/', views.names, name='names'),
    path('names/rait/', views.rait, name='rait'),
    path('names/rait/rezu/', views.rezu, name='rezu'),
    path('names/rait/rezu/id', views.create, name='newid'),
    path('ids', views.set_in, name='ids'),
]