from django.db import models

class Infos(models.Model):
    id = models.AutoField('ID', serialize=False, primary_key=True, db_index= True, auto_created=True, unique= True)
    womens = models.TextField('Womens')
    mans = models.TextField('Mens')
    rait = models.TextField('Raiting')

    def __str__(self):
        return f"id: {self.id} womens: {self.womens}\n mans: {self.mans}\nrait {self.rait}"

    def get_w(self):
        return self.womens

    def get_m(self):
        return self.mans

    def get_r(self):
        return self.rait
