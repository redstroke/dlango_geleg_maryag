from .models import Infos
from django.forms import ModelForm

class InfoForm(ModelForm):
    class Meta:
        model = Infos
        fields = ['womens', 'mans', 'rait']