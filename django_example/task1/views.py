from django.shortcuts import render
from django.template.defaulttags import register
from .models import Infos

# class Names(object):
#     def __init__(self, id, fio):
#         self.id = id
#         self.info = fio
#
#     def __repr__(self):
#         return f"{self.info}"


class Info(object):
    def __init__(self, id, w, m, rai):
        self.id = id
        self.women = list(w)
        self.men = list(m)
        self.raiting = list(rai)

    def __repr__(self):
        return f"{self.raiting}"

    def get_kol(self):
        return len(self.women)

    def get_namesW(self):
        return self.women

    def get_namesM(self):
        return self.men

    def get_range(self):
        return self.raiting

    def set_range(self, r):
        self.raiting = r

    def set_namesW(self, r):
        self.women = r

    def set_namesM(self, r):
        self.men = r

# class Range(object):
#     def __init__(self, id, r):
#         self.id = id
#         self.info = list(r)
#
#     def __repr__(self):
#         return f"{self.r}"
#
#     def get_range(self):
#         return self.info

# wom = ['Dasha', 'Lora', 'Gven']
# ma = ['Mark', 'Aleksis', 'Sten']
# Ra = []
wom = []
ma = []
I = []
I0 = Info(1,[], [], [])
flag = 0
def index(request):
    # ma = ['Mark', 'Aleksis', 'Sten']
    if (flag != 0):
        k = Info.get_kol(I0)
    else:
        k = ''
    return render(request, 'task1/homePage.html', {'kol': k})

def names(request):
    print(request.POST)
    k = int(request.POST['kol'])
    if (flag != 0):
        k = Info.get_kol(I0)
        ww = Info.get_namesW(I0)
        mm = Info.get_namesM(I0)
    else:
        ww = []
        mm = []
        for i in range(k):
            ww.append('')
            mm.append('')
    return render(request, 'task1/names.html', {'kolvo': k, 'w': ww, 'm': mm})

def create(request):
    print('RRRRRRR')
    ww = Info.get_namesW(I0)
    mm = Info.get_namesM(I0)
    rr = Info.get_range(I0)
    # w = ''
    # m = ''
    # r = ''
    # for w1 in ww:
    #     w = w + w1 + ','
    #
    # for m1 in mm:
    #     m = m + m1 + ','
    #
    # for r1 in rr:
    #     r = r + str(r1) + ' '

    I1 = Infos.objects.create(womens=ww, mans=mm, rait=rr)
    ind = I1.id
    return render(request, 'task1/okno.html', {'ind': ind})


def set_in(request):
    ind = request.POST.getlist('ind')
    i = Infos.objects.get(id = ind[0])

    m = i.get_m()
    w = i.get_w()
    r = i.get_r()
    mm = ''
    ww = ''
    rr = ''
    for j in m:
        if ((j != '[') & (j != "'") & (j != ']')):
            mm = mm + j
    for j in w:
        if ((j != '[') & (j != "'") & (j != ']')):
            ww = ww + j
    for j in r:
        if ((j != '[') & (j != "'") & (j != ']')):
            rr = rr + j

    ma = mm.split(', ')
    wom = ww.split(', ')
    rai1 = rr.split(', ')
    rai =[]
    for r1 in rai1:
        rai.append(int(r1))

    Info.set_range(I0, rai)
    Info.set_namesM(I0, ma)
    Info.set_namesW(I0, wom)
    # I.append(Info(0, wom, ma, rai))
    global flag
    flag = 1
    return index(request)

def rait(request):
    print(request.POST)
    wom1 = request.POST.getlist('woman')
    ma1 = request.POST.getlist('man')
    Info.set_namesM(I0, ma1)
    Info.set_namesW(I0, wom1)
    if (flag != 0):
        if (len(Info.get_range(I0)) != 0):
            n = Info.get_range(I0)
    else:
        # I.append(Info(0, wom1, ma1, []))
        # for mm in wom1:
        #     P.append(Names(0, mm))
        # for mm in ma1:
        #     P.append(Names(1, mm))
        k = len(wom1)*len(wom1)*len(wom1)*len(wom1)
        n = []
        for i in range(k):
            n.append('')
    print(n)
    l = len(ma1)
    ind = 0
    Rw = []
    Rm = []
    for i in range(l):
        rr = []
        for j in range(l):
            rr.append(n[ind])
            ind = ind + 1
        Rw.append(rr)
    for i in range(l):
        rr = []
        for j in range(l):
            rr.append(n[ind])
            ind = ind + 1
        Rm.append(rr)
    print(Rm)
    print(Rw)
    return render(request, 'task1/rait.html', {'kol': l, 'women': wom1, 'men': ma1, 'raitW':Rw, 'raitM':Rm})

def rezu(request):
    print(request.POST)
    r = request.POST.getlist('raiting')
    rai = []
    for pa in r:
        rai.append(int(pa))
    Ra = []
    Info.set_range(I0,rai)
    # Ra.append(Range(0, rai))
    pari = mariag(rai)
    # Info.set_range(I0, rai)
    # Info.set_namesM(I0, Info.get_namesM(I[0]))
    # Info.set_namesW(I0, Info.get_namesW(I[0]))
    I.clear()
    global flag
    flag = 0
    # Ra.clear()
    return render(request, 'task1/rezu.html', {'pari': pari})

@register.filter
def get_range(value):
    return range(value)

@register.filter
def get_in(value, arg):
    return value[arg]


def ex(request):
    I.append(Info(0, ['Ann', 'Dora', 'Ally'], ['Break', 'Tom', 'Ostin'], [1,2,3,2,1,3,3,2,1,1,3,2,1,2,3,2,1,3]))
    Info.set_range(I0, [1,2,3,2,1,3,3,2,1,1,3,2,1,2,3,2,1,3])
    Info.set_namesM(I0,  ['Break', 'Tom', 'Ostin'])
    Info.set_namesW(I0, ['Ann', 'Dora', 'Ally'])
    global flag
    flag = 1
    # I.append(Info(1, ['Break', 'Tom', 'Ostin']))
    # Ra.append(Range(0,[1,2,3,2,1,3,3,2,1,1,3,2,1,2,3,2,1,3]))
    return index(request)

def mariag(r):
    # wom = []
    # ma = []
    # print(P)
    # l1 = len(P)
    # l = int(l1/2)
    # print(l)
    # for i in range(l):
    #     wom.append(str(P[i]))
    # for i in range(l, l1):
    #     ma.append(str(P[i]))
    # wom = []
    # ma = []
    wom = Info.get_namesW(I0)
    ma = Info.get_namesM(I0)
    l = Info.get_kol(I0)
    print(wom)
    print(ma)
    ind = 0
    Rw = []
    Rm = []
    for i in range(l):
        rr = []
        for j in range(l):
            rr.append(r[ind])
            ind = ind + 1
        Rw.append(rr)
    for i in range(l):
        rr = []
        for j in range(l):
            rr.append(r[ind])
            ind = ind + 1
        Rm.append(rr)
    print(Rw)
    print(Rm)
    W = []
    M = []
    Wf = []
    for i in range(l):
        W.append(-1)
        M.append(-1)
        Wf.append(0)
    km = l
    while km > 0:
        for i in range(l):
            if M[i] == -1:
                f = 0
                prefW = -1
                for j in range(l):
                    if ((Wf[j] == 0) & (f == 0)):
                        prefW = j
                        f = 1
                for j in range(l):
                    if ((Wf[j] == 0) & (Rm[i][j] < Rm[i][prefW])):
                        prefW = j
                print(Wf)
                print(prefW)
                print(Rw[prefW])
                print(Rm[i])
                if (prefW != -1):
                    if W[prefW] == -1:
                        W[prefW] = i
                        M[i] = prefW
                        km = km - 1
                    else:
                        if (Rw[prefW][W[prefW]] > Rw[prefW][i]):
                            M[W[prefW]] = -1
                            W[prefW] = i
                            M[i] = prefW
                    f = 0
                    prefM = -1
                    for j in range(l):
                        if ((M[j] == -1) & (f == 0)):
                            prefM = j
                            f = 1
                    for j in range(l):
                        if ((M[j] == -1) & (Rw[prefW][j] < Rw[prefW][prefM])):
                            prefM = j
                    if prefM == i:
                        Wf[prefW] = 1

    print(M)
    print(W)
    rez = []
    for i in range(l):
        st = wom[i] + ' и ' + ma[W[i]]
        rez.append(st)
    return rez

